Famous Four Media Technical Challenge
=================================
The technical challenge consists on building a simple but fully functional **Contact Book**. Users will be able to do any of the following actions within the application. 

- Add a new contact
- Edit a contact
- Remove a contact
- List contacts
- Search contacts

A contact has at least the following data: name, surname, email address and phone number (optional). All the data has to be formatted correctly.

A contact can belong to one or more groups. For example: "Maria" and "Juan" can belong to the group "Colleagues" and "Juan" can also belong to the group "Football Friday".

The contact search will allow to search by any data related to a contact (including groups).

All contacts **need to be persisted** so it is available for future use.

Rules
-------
You need to comply with the following rules for the resolution of the technical challenge.

- It has to be a web-based application 
- It has to be fully functional
- You need to develop this application as it was ready for production
- Deployment/installation instructions need to be provided together with the source code

Please bear in mind that if the application does not work, we will consider the technical challenge as **failed** so please make sure to include all required information. 

Technology
--------------
You are free to use any technology/library/framework that you want. The only restriction is that the underlying programming language needs to be **PHP and Javascript**. We are very strict with notation and code styling. We follow **PSR-2** and camel case notation in all of our projects. Some technology that we use at Famous Four Media:

- Laravel 5.2
- ReactJS
- Doctrine
- Twig
- MySQL
- Composer
- SASS
- Gulp
- Browserify
- Monolog
- Coffee
- Pizza

Submission
--------------
Once you have your application ready, you can submit it via two methods:

- Providing a URL to a Github/Bitbucket repository containing your application *(preferred)*
- Sending us your source code as a compressed file via email. Please, **do not include** vendor or node modules folder.

In both cases, please make sure to include all necessary instructions for running your application.